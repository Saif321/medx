// controllers/productController.js
import productModel from '../models/productModel.mjs';

export const  getAllProducts = async (req, res) => {
  try {
    const result= await productModel.getAllProducts();
    res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
 
};

 export const getProductById = async (req, res) => {
  const { productId } = req.params;
  try {
    console.log('ddddddddddddddddddddddd',productId)
     await productModel.getProductById(productId,(err,result)=>{
     if (err)  res.status(404).json({ error: 'Product not found' });
     res.json(result);
    });
    
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

export const createProduct = async (req, res) => {
  const productData = req.body;
  try {
    const productId = await productModel.createProduct(productData);
    res.status(201).json({ message: 'Product created successfully', productId });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

export const updateProduct = async (req, res) => {
  // const { productId } = req.params;
  const productData = req.body;
  try {
     await productModel.updateProduct(productData, (err,result)=>{
      if (err) {
        
        res.status(500).json({ error: 'inernal server error' });
      }else{
        if (!result) {
          res.status(404).json({ error: 'Product not found' });
        }
        else{
          res.json({ message: 'Product updated successfully' });
          
        }
      }

    });
   
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

export const deleteProduct = async (req, res) => {
  const { productId } = req.params;
 
  try {
     await productModel.deleteProduct(productId,(err,result)=>{
      if (err) {
        res.status(404).json({ error: 'Internal server error' });
      }
      else if(!result){
        res.status(404).json({ error: 'Product not found' });
      }
      
      res.json({ message: 'Product deleted successfully' });
    });
   
    
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};


