// src/controllers/favoriteController.js
import FavoriteModel from '../models/favoriteModel.mjs';

// Get all favorite medicines
export const getAllFavorites = (req, res) => {
  FavoriteModel.getAllFavorites((error, results) => {
    if (error) {
      res.status(500).json({ error: 'Internal server error' });
      return;
    }
    res.json(results);
  });
};

// Get a single favorite medicine by ID
export const getFavoriteById = (req, res) => {
  const { id } = req.params;
  FavoriteModel.getFavoriteById(id, (error, result) => {
    if (error) {
      res.status(404).json({ message: error });
      return;
    }
    res.json(result);
  });
};

// Add a medicine to the user's favorites
export const addFavorite = async (req, res) => {
  const { user_id,	product_id } = req.query;
   const favorite={user_id,product_id}
  FavoriteModel.addFavorite(favorite, (error, result) => {
    if (error) 
      res.status(500).json({ error: 'Internal server error' });
     
    
    res.status(201).json({ message: 'Medicine added to favorites' });
  });
};

// Remove a medicine from the user's favorites
export const removeFavorite =async (req, res) => {
  const { id } = req.params;
  
  await FavoriteModel.removeFavorite(id, (error, result) => {
    if (error) 
      res.status(404).json({ message: "Cant deleted" });
      
    
    res.json({ message: 'Medicine removed from favorites',deletedFav:result });
  });
};
