// controllers/userController.mjs
import User from '../models/userModel.mjs';
import * as bcrypt from 'bcrypt';


 export const getAllUsers= async (req, res) => {
    try {
     User.getAllUsers((users)=>{
        res.json(users);
      });
     
    } catch (err) {
      console.error('Error fetching users=', err);
      res.status(500).json({ error:'Internal server error' });
    }
  }

 export const getUserById= async (req, res) => {
    const id =parseInt(req.params.id) ;
    
    try {
      
       await User.getUserById(id,(err,result)=>{
        if (err)  res.status(500).json({ error: err });
        else if (!result) {
          res.status(404).json({ error: 'User not found' });
        } else {
          res.json(result);
        }
       });
     
    } catch (err) {
      console.error('Error fetching user by ID=', err);
      res.status(500).json({ error: 'Internal server error' });
    }
  }

  export const createUser = async (req, res) => {
    const {
      name,
      phone,
      address,
      email,
      password,
    } = req.query;
  
    const userData = { name, phone, address, email, password };
  
    try {
      User.createUser(userData, (err, userId) => {
        if (err) {
          res.status(500).json({ error: 'Internal server error' });
        } else {
          res.status(201).json({ message: 'User created successfully', userId });
        }
      });
    } catch (error) {
      console.error('Controller error:', error);
      res.status(500).json({ error: 'Internal server error' });
    }
  };
  export const loginUser = (req, res) => {
    const { email, password } = req.query;
  
    try {
      User.logingUser(email, password, (error, user) => {
        if (error) {
          res.status(401).json({ message: error });
        } else {
          res.status(201).json({ message: 'Logged in successfully', user });
        }
      });
    } catch (error) {
      console.error('Controller error:', error);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  };
 export const updateUser= async (req, res) => {
    const userId = req.params.id;
    const userData = req.query;
    try {
      await User.updateUser(userId, userData);
      res.json({ message: 'User updated successfully' });
    } catch (err) {
      console.error('Error updating user=', err);
      res.status(500).json({ error: 'Internal server error' });
    }
  }

  


