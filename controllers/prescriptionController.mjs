import prescriptionModel from '../models/prescription_model.mjs' // Adjust the path as needed



export const getAllPrescriptionsController = async (req, res) => {
  try {
    const prescriptions = await prescriptionModel.getAllPrescriptions();
    res.status(200).json(prescriptions);
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
};

export const createPrescriptionController = async (req, res) => {
  const {user_id } = req.query;
  try {
    const result = await prescriptionModel.createPrescription(user_id);
    res.status(201).json({ id: result.insertId });
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
};

export const deletePrescriptionController = async (req, res) => {
  const { id } = req.params;
  try {
    const result = await prescriptionModel.deletePrescription(id);
    res.status(200).json({ message: 'Prescription deleted successfully',deletedItem:result });
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
};
////
export const getPrescriptionsById=async(req,res)=>{
  const {id}=req.params;
  try {
      const result= await prescriptionModel.getPrescriptionsById(id);
      res.status(200).json(result);
  } catch (error) {
    res.status(500).json({error:'Internal Server error'});
  }
}
////// getPrescription details 
export const getPrescriptionDetails=async(req,res)=>{
   const {pid}=req.params;
   try {
      const result= await prescriptionModel.getPrescriptionDetails(pid);
      res.status(200).json(result);
   } catch (error) {
    res.status(500).json({error:'Internal Server error'});
   }
}
export const insertImageDetails= async(req,res)=>{
  const {prescription_id,image}= req.query;
 
   console.log('_______>',prescription_id,image);
  try {
    const result= await prescriptionModel.insertImageDetails(prescription_id,image);
    res.status(200).json({insertedData:result});
  } catch (error) {
    res.status(500).json({error:error.toString()});
  }
}


