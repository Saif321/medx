// routes/userRoute.mjs
import express from 'express';
import  { getAllUsers, getUserById, createUser,updateUser,loginUser } from '../controllers/userController.mjs';

import { addFavorite,getAllFavorites,getFavoriteById,removeFavorite } from '../controllers/favoriteController.mjs';
import {getAllProducts,createProduct,updateProduct,deleteProduct,getProductById} from '../controllers/productController.mjs';
import { getAllPrescriptionsController,createPrescriptionController,deletePrescriptionController, getPrescriptionsById, insertImageDetails, getPrescriptionDetails } from '../controllers/prescriptionController.mjs';
import { insertCart } from '../controllers/cartController.mjs';
const router = express.Router();
// user routes
router.get('/users', getAllUsers);
router.get('/users/:id', getUserById);
router.post('/register', createUser);
router.put('/users/:id', updateUser);
router.post('/loging', loginUser);
// product
router.get('/products', getAllProducts);
router.get('/products/:productId', getProductById);
router.post('/products', createProduct);
router.put('/products/:productId', updateProduct);
router.delete('/products/:productId', deleteProduct);
// favorite routes
router.get('/favorites', getAllFavorites);
router.get('/favorites/:id',getFavoriteById);
router.post('/favorites', addFavorite);
router.delete('/favorites/:id', removeFavorite);
// prescription routes
// GET all prescriptions
router.get('/prescriptions', getAllPrescriptionsController);

// POST a new prescription
router.post('/prescriptions', createPrescriptionController);

// DELETE a prescription by ID
router.delete('/prescriptions/:id', deletePrescriptionController);
//getPrescripion by user id
router.get('/prescription/:id',getPrescriptionsById);
//insert image detail of prescription
router.post('/prescriptionDetail',insertImageDetails);
// get prescription details with images url
router.get('/pres/:pid',getPrescriptionDetails)
// cart routes

router.post('/cart',insertCart);



export default router;
// Registration route
