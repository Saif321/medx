
import db from "../databaseconfig.mjs";
class ProductModel {
  getAllProducts(){
 

  try {
  
  return new Promise((resolve,reject)=>{
    db.getConnection((err,connection)=>{
      if (err) {
        reject(err);
        
     }
     else{
       
      connection.query('SELECT * FROM product',(qryErr,result)=>{
        if (qryErr) {
          reject(qryErr)
          
        }
        else{
           resolve(result);
           connection.release(); // Release the connection.
           console.log('Released Connection');

        }
      });
     }
       

     
    })
    
  });
  
  } catch (error) {
     throw error;
  }
};

 getProductById = async (productId,callback) => {
  db.getConnection((err,connection)=>{
    if (err) {
      callback(err,null)
    }
    else{
      connection.query('SELECT * FROM product WHERE productId = ?', [productId],(err,result)=>{
        if (err) callback(err,null)
        callback(null,result)
        connection.release()
        console.log('ReleasedConnection');
       });
     
    }
  })
   
 
};

 createProduct = async (productData,callback) => {
  const { genericName, description, dosageForm, image, title, unitType, category, subCategory, activeStatus, Ratings } = productData;
   const status=parseInt(activeStatus);
   const rate=parseInt(Ratings)

  db.getConnection((err,connection)=>{
    if (err) {
      callback(err,null)
    }
    else{
      connection.query(
        'INSERT INTO product (genericName, description, dosageForm, image, title, unitType, category, subCategory, activeStatus, Ratings) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
        [genericName, description, dosageForm, image, title, unitType, category, subCategory, status, rate],(err,result)=>{
         if(err)  callback(err,null)
         callback(null,result.insertId) 
         connection.release()
         console.log('ReleasedConnection');
        }
      );
  
    }
  })
  
};

 updateProduct = async (productData, callback) => {

  const {productId, genericName, description, dosageForm, image, title, unitType, category, subCategory, activeStatus, Ratings } = productData;
  const status=parseInt(activeStatus);
  const rate=parseInt(Ratings);
  const id=parseInt(productId);
  db.getConnection((err,connection)=>{
    if (err) {
      callback(err,null)
    }
    else{connection.query(
      'UPDATE product SET genericName = ?, description = ?, dosageForm = ?, image = ?, title = ?, unitType = ?, category = ?, subCategory = ?, activeStatus = ?, Ratings = ? WHERE productId = ?',
      [ genericName, description, dosageForm, image, title, unitType, category, subCategory, status, rate, productId],((err,result)=>{
      if (err) {
        callback(err,null)
      }
      callback(null,result)
      connection.release()
  console.log('ReleasedConnection');
      })
    );
      
    }
  }) 
  
  
};

 deleteProduct = async (productId,callback) => {
  db.getConnection((err,connection)=>{
    if (err) {
      callback(err,null)
    }
    else{
       connection.query('DELETE FROM product WHERE productId = ?', [productId],(err,result)=>{
        if (err) {
          callback(err,null);
          
        }else{
          callback(null,result);
          connection.release()
          console.log('ReleasedConnection');
        }
      });
     
    }
  })
   
  
};


}

export default new ProductModel();
