// src/models/FavoriteModel.js
import db from '../databaseconfig.mjs'; // Assuming you have a MySQL connection

class FavoriteModel {
  // Get all favorite medicines
  //  getAllFavorites(callback) {
  //   db.getConnection((err,connection)=>{
  //     if (err) {
  //       callback(err,null)
  //     }
  //     else{
  //       connection.query('SELECT * FROM favorite', (error, results) => {
  //         if (error) {
  //           callback(error, null);
  //           return;
  //         }
  //         callback(null, results);
  //       });
  //       connection.release()
  //   console.log('ReleasedConnection');
  //     }
  //   })
   
  // }

  // Get a single favorite medicine by ID
   getFavoriteById(id, callback) {
    db.getConnection((err,connection)=>{
      if (err) {
        callback(err,null)
      }
      else{
        connection.query('SELECT * FROM favorite WHERE user_id = ?', [id], (error, results) => {
          if (error) {
            callback(error, null);
            
          }
          if (results.length === 0) {
            callback('Favorite medicine not found', null);
            
          }
          callback(null, results[0]);
        });
        connection.release()
    console.log('ReleasedConnection');
      }
    })
    
  }

  // Add a medicine to the user's favorites
  addFavorite= async(favorite, callback) =>{
    const userid = parseInt(favorite.user_id);
    const medicineid = parseInt(favorite.product_id);
  
    const sqlq = 'INSERT INTO favorite (user_id, product_id) VALUES (?, ?)';
    const VALUES = [userid, medicineid];
    db.getConnection((err,connection)=>{
      if (err) {
        callback(err,null)
      }
      else{
        
    connection.query(sqlq, VALUES, (error, results) => {
      if (error) callback(error, null);
  
      
      callback(null, results);
      connection.release()
      console.log('ReleasedConnection');
    });
      
      }
    })
  
  }

  // Remove a medicine from the user's favorites
   removeFavorite=async(id,callback) =>{
     console.log('favorite____________________',id)
    const favorite=parseInt(id);
     const sqlq=`DELETE FROM favorite WHERE favorite_id = ?`;
    const values=[favorite];
    db.getConnection((err,connection)=>{
      if (err) {
        callback(err,null)
      }
      else{
        connection.query(sqlq, values, (error, results) => {
          if (error) callback(error, null);
          callback(null, results);
        });
        connection.release()
    console.log('ReleasedConnection');
      }
    })
    
  
  }
}

export default new FavoriteModel();
