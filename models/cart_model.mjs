
import pool from "../databaseconfig.mjs";

class CartModel{
     getCartByid(id){
      const  userid= parseInt(id);
         return new Promise((resolve,reject)=>{
            pool.getConnection((err,connection)=>{
                if (err) {
                    reject(err);
                }
                else{
                    connection.query('select* FROM cart WHERE user_id=?',[user4],(qryError,result)=>{
                        if (qryError) {
                            reject(qryError);
                        }
                        else{
                            resolve(result);
                            connection.release();
                            console.log('connection released');
                        }
                    })
                }
            })
         });
     }
     deleteCartByUserId(id){
        const  userid= parseInt(id);
           return new Promise((resolve,reject)=>{
              pool.getConnection((err,connection)=>{
                  if (err) {
                      reject(err);
                  }
                  else{
                      connection.query('DELETE FROM cart WHERE user_id=?',[user4],(qryError,result)=>{
                          if (qryError) {
                              reject(qryError);
                          }
                          else{
                              resolve('DELETED ITEMS',result);
                              connection.release();
                              console.log('connection released');
                          }
                      })
                  }
              })
           });
       }

       deleteOneItemFromCart(itemId){
        const  id= parseInt(itemId);
           return new Promise((resolve,reject)=>{
              pool.getConnection((err,connection)=>{
                  if (err) {
                      reject(err);
                  }
                  else{
                      connection.query('DELETE* FROM cart WHERE product_id=?',[id],(qryError,result)=>{
                          if (qryError) {
                              reject(qryError);
                          }
                          else{
                              resolve('item Deleted',result);
                              connection.release();
                              console.log('connection released');
                          }
                      })
                  }
              })
           });
       }


       insertCart(userData){
    const   { user_id ,    
        product_id ,   
        quantity }= userData;
       const userId=parseInt(user_id);
       const productId=parseInt(product_id);
       const qnty=parseInt(quantity);
        
        return new Promise((resolve,reject)=>{
            pool.getConnection((err,connection)=>{
                if (err) {
                    reject(err);
                }
                else{
                    const insertQuery = 'INSERT INTO carts (user_id, ) VALUES (?)';
                    connection.query(insertQuery,[userId],(qryError,result)=>{
                        if (qryError) {
                            reject(qryError);

                            
                        }
                        else{
                           const cartId=result.insertId;
                            connection.query('insert into cartitems(cart_id,	product_id,	quantity) VALUES(?, ?, ?)',[cartId,productId,qnty],(qryError,result)=>{
                                if (qryError) {
                                    console.log('"Querry error of inserting cart items"',qryError)
                                   
                                }
                                else{
                                    resolve(result)
                                }
                            });
                            // resolve(result);
                            connection.release();
                            console.log('Connection released')
                        }
                    })
                }
            })
        })
       }
       
       updateCartItem(userData){
        const   {    
            product_id ,   
            quantity }= userData;
           //const userId=parseInt(user_id);
           const productId=parseInt(product_id);
           const qnty=parseInt(quantity);
            
            return new Promise((resolve,reject)=>{
                pool.getConnection((err,connection)=>{
                    if (err) {
                        reject(err);
                    }
                    else{
                        const updateQuery = 'UPDATE cart SET quantity = ? WHERE cart_id = ?';
                        connection.query(updateQuery,[,qnty,productId],(qryError,result)=>{
                            if (qryError) {
                                reject(qryError);
    
                                
                            }
                            else{
                                resolve(result);
                                connection.release();
                                console.log('Connection released')
                            }
                        })
                    }
                })
            })
           }

}
export default new CartModel();