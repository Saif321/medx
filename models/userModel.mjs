import mysql from 'mysql2/promise';
import db from '../databaseconfig.mjs';
import * as bcrypt from 'bcrypt';
import pool from '../databaseconfig.mjs';
// const { jwt } ='jsonwebtoken';



// User model
class User  {
  getAllUsers (callback)  {
    db.getConnection((err,connection)=>{
      if (err){ callback(err)}
      else {
        const query = 'SELECT * FROM user_details';
      connection.query(query,(error,result)=>{
      if(error) throw error
       callback(result)
       connection.release();
       
       console.log('connection released')
 
     });
      } 
      
    })
   
  }
  getUserById= async (id,callback) => {
    console.log('id ====',id)
    const userid=parseInt(id);
    db.getConnection((err,connection)=>{
      if(err) callback("cant connect with server",null)
     connection.query('SELECT * FROM user_details WHERE user_id = ?', [userid],(err,result)=>{
    if (err) callback(err,null)
    callback(null,result[0])
    connection.release();
  
  });
      
    })
   
  }
  createUser = async (userData, callback) => {
    const { name, phone, address, email, password } = userData;
  
    try {
      // Hash the password
      const hashedPassword = await bcrypt.hash(password, 10);

      db.getConnection((err,connection)=>{
        if (err) {
          callback(err,null)
        }
        connection.query(
          'INSERT INTO user_details (name, phone, address, email, password) VALUES (?, ?, ?, ?, ?)',
          [name, phone, address, email, hashedPassword],
          (err, result) => {
            if (err) {
              return callback(err, null);
            }
            callback(null, result);
            connection.release();
            console.log("conncetion released")
          }
        );
      })
  
      // Insert user data into the database
      
    } catch (error) {
      console.error('Error creating user:', error);
      callback(error, null);
    }
  };
  // loging
  logingUser = async (email, password, callback) => {
    try {
      // Retrieve user by email from the database
     db.getConnection((err,connection)=>{
      if (err) {
        callback("Inetnal Server err",null)
        
      }
      else{
        connection.query('SELECT * FROM user_details WHERE email = ?', [email], async (err, result) => {
          if (err) {
            callback(err, null);
          } else if (!result || result.length === 0) {
            callback('User not found', null);
          } else {
            const hashedPassword = result[0].password;
    
            // Compare the given password to the hashed password stored in the database
            const passwordMatch = await bcrypt.compare(password, hashedPassword);
    
            if (!passwordMatch) {
              callback('Invalid password', null);
            } else {
              callback(null, result[0]);
              connection.release()
              console.log('Connection Released') // Pass the user object, not the entire result
            }
          }
        });
      }
     })
    } catch (error) {
      console.error('Login error:', error);
      callback('Internal Server Error', null);
    }
  };

  // createUser= async (userData,callback) => {
  //   const {name,phone,address,email,password,role}=userData;
  //   const VALUES=[name,phone,address,email,password,role];
  //   const query='insert into users (name, phone, address, email, password, role) values(?, ?, ?, ?, ?, ?)';
  //    connection.query(query,(err,result=>{
  //     if (err)  callback(err,null);
  //       callback(result);
  //   }));
    
  // }

  updateUser = async (id, userData) => {
    const { name, email, phone, address, password } = userData;
    const values = [];
    const updates = [];
  
    if (name) {
      updates.push('name = ?');
      values.push(name);
    }
  
    if (email) {
      updates.push('email = ?');
      values.push(email);
    }
  
    if (phone) {
      updates.push('phone = ?');
      values.push(phone);
    }
  
    if (address) {
      updates.push('address = ?');
      values.push(address);
    }
  
    if (password) {
      const hashedPassword = await bcrypt.hash(password, 10);
      updates.push('password = ?');
      values.push(hashedPassword);
    }
  
    if (values.length === 0) {
      console.log('No fields to update.');
      return;
    }
  
    values.push(id);
  
    const updateSetClause = updates.join(', ');
    db.getConnection((err,connection)=>{
      if (err) {
        callback(err,null)
      }
      else{
        const query = `UPDATE user_details SET ${updateSetClause} WHERE user_id = ?`;
  
    connection.query(query, values, (err, result) => {
      if (err) {
        console.error('Error updating user:', err);
      } else {
        console.log('User updated successfully');
        connection.release();
        console.log('connection released')
      }
    });
      }
    })
    
  };

  deleteUser= async (id) => {
    db.getConnection((err,connection)=>{
      if (err) {
        callback(err,null)
      }
      else{
        connection.query('DELETE FROM user_details WHERE user_id = ?', [id]);
        connection.release()
    console.log('ReleasedConnection');
      }
    })
    
  }
}


export default new User();
