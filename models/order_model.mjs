const query = `
  SELECT
    O.order_id,
    O.user_id,
    U.name AS user_name,
    O.order_date,
    O.order_status,
    O.payment_method,
    O.is_payment_paid,
    GROUP_CONCAT(CONCAT(P.title, ' (', I.quantity, ' units)') SEPARATOR ', ') AS ordered_products
  FROM Orders AS O
  JOIN user_details AS U ON O.user_id = U.user_id
  JOIN order_items AS I ON O.order_id = I.order_id
  JOIN product AS P ON I.product_id = P.productId
  WHERE O.order_id = ?
  GROUP BY O.order_id;
`;

// Execute the query
connection.query(query, [desiredOrderId], (err, results) => {
  if (err) {
    console.error('Error retrieving order details:', err);
    connection.end();
    return;
  }

  if (results.length > 0) {
    const orderDetails = results[0];
    // Parse the 'ordered_products' column into an array
    const orderedProductsArray = orderDetails.ordered_products.split(', ');

    // Create a JSON object to hold the order details
    const orderJSON = {
      order_id: orderDetails.order_id,
      user_id: orderDetails.user_id,
      user_name: orderDetails.user_name,
      order_date: orderDetails.order_date,
      order_status: orderDetails.order_status,
      payment_method: orderDetails.payment_method,
      is_payment_paid: orderDetails.is_payment_paid,
      ordered_products: orderedProductsArray,
    };

    // Convert the order details to a JSON string
    const orderJSONString = JSON.stringify(orderJSON);

    console.log(orderJSONString);
  } else {
    console.log('Order not found.');
  }

  // Close the database connection
  connection.end();
});