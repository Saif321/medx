
import pool from "../databaseconfig.mjs";
class PrescriptionModel{
     getAllPrescriptions()  {
        return new Promise((resolve,reject)=>{
            pool.getConnection((err,connection)=>{
                if (err) {
                    reject(err);
                }else{
                    connection.query('SELECT * FROM prescription', (qryErr, results) => {
                        if (qryErr) {
                          reject(qryErr)
                        }
                        else{
                            resolve(results);
                            connection.release();
                        }
                      });
                }         
            })
        })
      };

    createPrescription(user_id){

         const id=parseInt(user_id);

        try {
            return new Promise((resolve,reject)=>{
             pool.getConnection((err,connection)=>{
                if (err) {
                    reject(err);
                }
                else{
                    connection.query('insert into prescription(user_id) VALUES(?)',[id],(qryErr,result)=>{
                        if (qryErr) {
                            console.log('Query Error:',qryErr)
                            reject(qryErr)
                        }
                        else{
                            resolve(result);
                            connection.release();
                            console.log('Connection Released')
                        }
                    })
                }
             })
            });
        } catch (error) {
            reject(error);
            console.log('Something goes wring:',error)
            
        }
       
    }
    deletePrescription(id){
        const  pId=parseInt(id);
          console.log('?????????????????????',pId);
          return new Promise((resolve,reject)=>{
              pool.getConnection(
                  (err,connection)=>{
                      if (err) {
                             reject(err);
                      }
                      else{
                          connection.query('delete from prescription where id=?',[pId],(qryErr,result)=>{
                              if (qryErr) {
                                  reject(qryErr)
                              }
                              else{
                                  resolve(result);
                              }
                          })
                      }
                  }
              );
          })
      }
      // get PrescriptionFolders details
      getPrescriptionsById(id)  {
       const userid=parseInt(id);
        return new Promise((resolve,reject)=>{
            pool.getConnection((err,connection)=>{
                if (err) {
                    reject(err);
                }else{
                    connection.query('SELECT * FROM prescription where user_id = ?',[userid], (qryErr, results) => {
                        if (qryErr) {
                          reject(qryErr)
                        }
                        else{
                            resolve(results);
                            console.log('Connection Has released')
                            connection.release();
                        }
                      });
                }         
            })
        })
      };
      // insert details with image in of prescription
      insertImageDetails(prescription_id,image){
       // console.log('imagedata',data);
        //prescription_id
     // const { prescription_id,	image,}=imageData;	
      const pid= parseInt(prescription_id);
     
    console.log('data',pid)
       try {
           return new Promise((resolve,reject)=>{
            pool.getConnection((err,connection)=>{
               if (err) {
                   reject(err);
               }
               else{
                   connection.query('insert into prescriptionfolder(prescription_id, image) VALUES(?, ?)',[pid,image],(qryErr,result)=>{
                       if (qryErr) {
                           console.log('Query Error:',qryErr)
                           reject(qryErr)
                       }
                       else{
                           resolve(result);
                           connection.release();
                           console.log('Connection Released')
                       }
                   })
               }
            })
           });
       } catch (error) {
           reject(error);
           console.log('Something goes wring:',error)
           
       }
      
   }

   getPrescriptionDetails(id){
      const pid=parseInt(id);
      console.log('Prescription_id======',pid)
      return new Promise((resolve,reject)=>{
        pool.getConnection((error,connection)=>{
            if (error) {
                reject(error)
            }
            else{
                const query=`SELECT
                p.id AS prescription_id,
                p.date_time AS prescription_date_time,
                p.user_id,
                p.status,
                GROUP_CONCAT(pf.image) AS folder_images
            FROM prescription AS p
            INNER JOIN prescriptionFolder AS pf ON p.id = pf.prescription_id
            WHERE p.id = ?
            GROUP BY p.id, p.date_time, p.user_id, p.status;
            `;         
                connection.query(query,[pid],(qryErr,result)=>{
                    if (qryErr) {
                        reject(qryErr);
                    }
                    else{
                        const parsedData = result.map(row => ({
                            prescription_id: row.prescription_id,
                            prescription_date_time: row.prescription_date_time,
                            user_id: row.user_id,
                            status: row.status,
                            folder_images: row.folder_images.split(',').map(image => image.trim())
                          }));
                      
                          const jsonData = JSON.stringify(parsedData);
                          const prescriptions = JSON.parse(jsonData);
                      
                          // Now you have your data in JSON format
                          console.log(prescriptions);
                        resolve(prescriptions);
                        console.log("connection Released")
                    }
                });
            }
        })
      })

   }
  

}
export default new PrescriptionModel();