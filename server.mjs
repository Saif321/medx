import express from 'express';
import userRoutes from "./routes/userRoute.mjs";

const app = express();

const PORT= 3001;

app.use(express.json());

app.use('/api', userRoutes);

app.listen(PORT, () => {
    console.log(`Server is running at http://localhost:${PORT}`);
});