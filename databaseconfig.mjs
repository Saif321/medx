import mysql from "mysql2";

// Create a connection pool
const pool = mysql.createPool({
  host: "localhost",
  user: "root",
  password: "",
  database: "pharmacy_store",
  waitForConnections: true, // Whether to queue up new connections if all connections are busy
  connectionLimit: 10, // Maximum number of connections in the pool
  queueLimit: 0, 
});

// Acquire a connection from the pool


export default pool;
